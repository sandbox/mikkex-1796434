<?php

/**
 * @file
 * Expermints with mustache.js (and mustache.php, soon)
 *
 */

/**
 * Idea, create to different renderers - one with mustache.js and the other one
 * with mustache.php, using the same template.
 * The template should be done in the ctools plugin configuration. Maybe added as
 * an style instead?
 *
 * This should be added to the project make file:
 * ; mustache.php
 * libraries[mustache.php][download][type] = git
 * libraries[mustache.php][download][url] = git://github.com/bobthecow/mustache.php.git
 * libraries[mustache.php][destination] = libraries
 *
 *; mustache.js
 * libraries[mustache.js][download][type] = get
 * libraries[mustache.js][download][url] = https://raw.github.com/janl/mustache.js/master/mustache.js
 * libraries[mustache.js][destination] = libraries\/
 */

$plugin = array(
  'title' => t('TITLE'),
  'description' => t('Plugin to render json as html from a template with mustache.js.'),
  'content_types' => array('render_json'),
   //  'defaults' => array('render_rss_render_json_YOUR_VAR' => ''),
  'category' => t('Renders'),
  'all contexts' => TRUE,
  'single' => TRUE,
);

/**
 * 'Edit' callback for the content type.
 */
function mustache_pane_render_json_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['render_rss_render_json_YOUR_VAR'] = array(
    '#type' => 'textfield',
    '#title' => t('TITLE'),
    '#description' => t('DESCRIPTION.'),
    '#default_value' => !empty($conf['mustache_pane_render_json_YOUR_VAR']) ? $conf['render_rss_render_json_YOUR_VAR'] : '',
  );
  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function mustache_pane_render_json_content_type_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}


/**
 * Put the output for you content type in the blocks content.
 */
function mustache_pane_render_json_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  // getJSON uses jQuery to fetch the JSON taht should be used (stored in var data)
  // var tpl is the template that are going to render the json
  // var html Creates html of the data and with template.
  //  "$('#sampleArea').html(html); Outputs the html in div with id sampleArea
  //  with jQuery.
  $js = "<script type='text/javascript'>" .
  '(function($) {
    $.getJSON("http://api.flickr.com/services/feeds/photos_public.gne?tags=kitten&tagmode=any&format=json&jsoncallback=?", function(data) {
    var tpl = "<h1>{{title}} {{link}}</h1>Blog: {{generator}}<br />" +
    "{{#items}}Image: {{title}}<br /> <strong>{{date_taken}}</strong><br />{{#media}} <img src={{m}} /> {{/media}}<br />{{/items}}";
    var html = Mustache.to_html(tpl, data);' .
    "$('#sampleArea').html(html);
    });
  }
  (jQuery))
  </script>";
  // Glue the content together
  $block->content = '<div id="sampleArea"></div>' . $js;
  return $block;
}
